const {ObjectId} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose.js');
const {Todo} = require('./../server/models/todo.js');
const {User} = require('./../server/models/user.js');

// Todo.remove({}).then((result) => {
//   console.log(result);
// });

// Todo.findOneAndRemove({_id: '5b3af21cb88bff0e8db4e0d3'}).then((todo) => {
//   console.log('todo');
// });

// Todo.findByIdAndRemove('5b3af1d4b88bff0e8db4e0a1').then((todo) => {
//   console.log(todo);
// });
