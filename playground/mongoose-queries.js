const {ObjectId} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose.js');
const {Todo} = require('./../server/models/todo.js');
const {User} = require('./../server/models/user.js');

// var id = '5b39b658f539439ac9cc2d933';

// if (!ObjectId.isValid(id)) {
//   console.log('ID not valid!');
// }

// Todo.find({
//   _id: id,
// }).then((todos) => {
//   console.log(' ');
//   console.log('Todos:', todos);
//   console.log(' ');
// });

// Todo.findOne({
//   _id: id,
// }).then((todo) => {
//   console.log(' ');
//   console.log('Todo:', todo);
//   console.log(' ');
// });

// Todo.findById(id).then((todo) => {
//   if (!todo) {
//     return console.log('Id not found!');
//   }
//   console.log(' ');
//   console.log('Todo by id:', todo);
//   console.log(' ');
// }).catch((e) => console.log(e));

var id = '5b396505cba9158e98a954cf';

User.findById(id).then((user) => {
  if (!user) {
    return console.log('User ID not found!');
  }
  console.log(' ');
  console.log(`User by ID: ${JSON.stringify(user, undefined, 2)}`);
  console.log(' ');
}).catch((e) => console.log(e));
